# Python Sample program

## Project Introduction

This project is using [Flask](http://flask.pocoo.org) framework simple to write [Python](https://www.python.org) sample program, directory structure：

```
.
├── Procfile
├── README.md
├── hello.py
├── runtime.txt
└── requirements.txt
```

## Project requirements

Project must be used with [Pip](https://pip.pypa.io) to resolve dependencies，If there is no `requirements.txt` file in the project directory, you have to create one，otherwise the project will not be deployed.


> Tip: You can use `pip freeze > requirements.txt` Command to generate `requirements.txt` file

`requirements.txt` example：

```
Flask==0.9
Jinja2==2.7.2
Werkzeug==0.8.3
gunicorn==19.0.0
```

If you want the app to run, you also need a `Procfile` file，specify the start command of the application inside.

`Procfile` example：

```
web: gunicorn hello:app --log-file - --access-logfile - --error-logfile -
```

## Local test

* Installation [Python](http://python.org) with [Virtualenv](http://pypi.python.org/pypi/virtualenv)，[view reference documentation](http://install.python-guide.org)。
* Execute the following command to create a [Virtualenv](http://pypi.python.org/pypi/virtualenv) and start the project inside：

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
gunicorn hello:app
```
* Access <http://localhost:8000> see the result.
